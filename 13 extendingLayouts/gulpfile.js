const gulp = require('gulp');
const pug = require('gulp-pug');

gulp.task('default', ['views'], () => {
  //
});

gulp.task('views', function buildHTML() {
  return gulp
    .src('assets/views/**/*.pug')
    .pipe(
      pug({
        // options
      })
    )
    .pipe(gulp.dest('./public/html'));
});
